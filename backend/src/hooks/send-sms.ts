// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import twilio from 'twilio';

export default (options = {}): Hook => {
  return async (context: HookContext) => {

    const { result } = context;

    console.log( result );

    const accountSid: string = 'ACe7bd2dc556688a76310f8e6681923bf3';
    const authToken: string = '32aa1f7f2f7f68f07a321782111abbc2';
    const client = twilio(accountSid, authToken);

    // Send message using promise
    const promise = client.messages.create({
      from: '+18577633425',
      to: result.destinationNumber,
      body: result.destinationMessage
    });
    promise.then(message => {
      console.log('Created message using promises');
      console.log(message.sid);
    });

    return context;
  };
}
