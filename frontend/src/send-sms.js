import React, { Component } from 'react';
import client from './feathers';

class Sendsms extends Component {

    constructor(props) {
        super(props);
    
        this.state = {};
    }

    sendSmsFunc(ev) {
        const inputDestinationNum = ev.target.querySelector('[id="destinationNum"]');
        const inputMessage = ev.target.querySelector('[id="inputMessage"]');
        const destinationNumber = inputDestinationNum.value.trim();
        const destinationMessage = inputMessage.value.trim();
    
        console.log( "Destination Number: " + destinationNumber );
        console.log( "Input Message: " + destinationMessage );
    
        this.state.sendSms.create({
          destinationNumber,
          destinationMessage
        })
        .then(() => {
            inputDestinationNum.value = '';
            inputMessage.value = '';
        });
    
        ev.preventDefault();
        
        
      }
    
    componentDidMount() {
        const sendSms = client.service('send-sms');
    
        this.setState({sendSms});
    }

    render() {
        return(
        <div>
          <div className="py-5 text-center">
            <h2>Send SMS</h2>
          </div>
    
          <div className="row">
            <div className="col-md-12 order-md-1">
              <h4 className="mb-3">SMS</h4>
              
              <form onSubmit={this.sendSmsFunc.bind(this)} className="needs-validation" noValidate>
                <div className="row">
                  <div className="col-md-12 mb-3">
                    <label htmlFor="desc">Destination Number</label>
                    <input type="number" className="form-control" id="destinationNum" defaultValue="" required />
                    <div className="invalid-feedback">
                        A description is required.
                    </div>
                  </div>
                </div>
                
                <div className="row">
                  <div className="col-md-12 mb-3">
                    <label htmlFor="desc">SMS Message</label>
                    <input type="text" className="form-control" id="inputMessage" defaultValue="" required />
                    <div className="invalid-feedback">
                        A description is required.
                    </div>
                  </div>
                </div>
                
                <button className="btn btn-primary btn-lg btn-block" type="submit">Send SMS</button>
              </form>
              
            </div>
          </div>
    
          <footer className="my-5 pt-5 text-muted text-center text-small">
            <p className="mb-1">&copy; 2020 CPSC 2650</p>
          </footer>
        </div>
        );
      }
}

export default Sendsms;